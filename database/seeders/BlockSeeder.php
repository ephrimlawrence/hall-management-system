<?php

namespace Database\Seeders;

use App\Models\Block;
use App\Models\Floor;
use App\Models\Room;
use Illuminate\Database\Seeder;

class BlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Block::factory()
             ->has(Floor::factory()->count(10), 'floors')
             ->has(Room::factory()->count(100), 'rooms')
             ->count(70)
             ->create();
    }
}
