<?php

namespace Database\Seeders;

use App\Models\Hall;
use Illuminate\Database\Seeder;

class HallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        $data = [
            ['name' => 'Valco Hall', 'logo' => 'https://i2.wp.com/valcohall.com/wp-content/uploads/2016/02/12274777_945029622248849_6553491797506010284_n.jpg'],
            ['name' => 'Casford Hall', 'logo' => 'https://i1.wp.com/fillahq.com/wp-content/uploads/2020/03/casford-forum-20160515_062428.jpg']
        ];
        collect($data)->each(fn($row) => Hall::query()->create($row));
    }
}
