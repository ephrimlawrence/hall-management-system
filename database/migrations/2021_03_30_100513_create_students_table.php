<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->text('first_name')->nullable();
            $table->text('other_names')->nullable();
            $table->text('last_name')->nullable();
            $table->text('level')->nullable();
            $table->text('programme')->nullable();
            $table->text('department')->nullable();
            $table->text('image')->nullable();
            $table->text('index_number')->nullable();
            $table->string('phone')->nullable();
            $table->date('move_out_date')->nullable();
            $table->date('move_in_date')->nullable();

            $table->unsignedBigInteger('hall_id');
            $table->unsignedBigInteger('room_id');
            $table->unsignedBigInteger('staff_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('hall_id')->references('id')->on('halls')
                  ->cascadeOnDelete();
            $table->foreign('room_id')->references('id')->on('rooms')
                  ->nullOnDelete();
            $table->foreign('staff_id')->references('id')
                  ->on('users')->nullOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
