<?php

namespace Database\Factories;

use App\Models\Floor;
use App\Models\Room;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoomFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Room::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() : array
    {
        return [
            'name' => '' . $this->faker->numberBetween(1, 500) . '',
            'capacity' => $this->faker->numberBetween(1, 6),
            'floor_id' => Floor::query()->inRandomOrder()->first()->id
        ];
    }
}
