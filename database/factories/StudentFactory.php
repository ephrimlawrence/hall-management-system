<?php

namespace Database\Factories;

use App\Models\Hall;
use App\Models\Room;
use App\Models\Staff;
use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName(),
            'other_names' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'level' => collect(range(100, 600, 100))->random(),
            'staff_id' => Staff::query()->inRandomOrder()->first()->id,
            'programme' => $this->faker->word,
            'department' => $this->faker->word,
            'image' => "https://i.pravatar.cc/150?u={$this->faker->email()}",
            'index_number' => $this->faker->word,
            'room_id' => Room::query()->inRandomOrder()->first()->id,
            'hall_id' => Hall::query()->inRandomOrder()->first()->id,
        ];
    }
}
