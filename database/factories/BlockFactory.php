<?php

namespace Database\Factories;

use App\Models\Block;
use App\Models\Hall;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlockFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Block::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() : array
    {
        return [
            'name' => $this->faker->word(),
            'hall_id' => Hall::inRandomOrder()->first()
        ];
    }
}
