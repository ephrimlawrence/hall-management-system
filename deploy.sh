#!/bin/bash

echo "============================================================================"
echo -e ">>Executing deploy script at: $(date)\n"

function optimize_app() {
    composer dump-autoload -o

    php artisan optimize
    php artisan view:cache
}

function clear_cache() {
    php artisan config:clear
    php artisan cache:clear
    php artisan view:clear
    php artisan route:clear
    php artisan clear-compiled
}

if [[ "$1" == 'dev' ]]; then
    cd ~/demo/hall_management_system

    # fetch updates
    git fetch
    git merge origin/dev

    # install packages
    composer install --verbose --ignore-platform-reqs

    # laravel chores
    clear_cache
    php artisan migrate --force

    # optimize app
    optimize_app
fi

if [[ "$1" == 'production' ]]; then
    cd ~/production/hall_management_system

    # fetch updates
    git fetch
    git merge origin/production

    # install packages
    composer install --no-dev --verbose --ignore-platform-reqs

    # laravel chores
    clear_cache
    php artisan migrate --force

    # optimize app
    optimize_app
fi

if [[ "$1" == 'local' || "$1" == '' ]]; then
    # fetch updates
    git fetch
    git merge origin/dev

    # install packages
    composer install --verbose --ignore-platform-reqs

    # laravel chores
    clear_cache
    php artisan migrate --force
fi

echo -e "\n>>Script execution ended at: $(date)"
echo -e "============================================================================\n"
