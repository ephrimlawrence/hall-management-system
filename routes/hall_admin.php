<?php

use App\Http\Controllers\HallAdmin\DashboardController;
use App\Http\Controllers\HallAdmin\FloorsController;
use App\Http\Controllers\HallAdmin\RoomsController;
use App\Http\Controllers\HallAdmin\StaffController;
use App\Http\Controllers\HallAdmin\StudentsController;
use Illuminate\Support\Facades\Route;

Route::get('', [DashboardController::class, 'index'])->name('hall_admin.dashboard');

Route::prefix('staffs')->group(function () {
    Route::get('', [StaffController::class, 'index'])->name('hall_admin.staff.index');
    Route::match(['get', 'post'], 'create', [StaffController::class, 'create'])->name('hall_admin.staff.create');
});

Route::prefix('rooms')->group(function () {
    Route::get('', [RoomsController::class, 'index'])->name('hall_admin.room.index');
    Route::match(['get', 'post'], 'create', [RoomsController::class, 'create'])->name('hall_admin.room.create');
    Route::get('delete/{id}', [RoomsController::class, 'delete'])->name('hall_admin.room.delete');
});

Route::prefix('floors')->group(callback: function () {
    Route::get('', [FloorsController::class, 'index'])->name('hall_admin.floor.index');
    Route::match(['get', 'post'], 'create', [FloorsController::class, 'create'])->name('hall_admin.floor.create');
    Route::get('delete/{id}', [FloorsController::class, 'delete'])->name('hall_admin.floor.delete');
});

Route::prefix('students')->group(function () {
    Route::get('', [StudentsController::class, 'index'])->name('hall_admin.student.index');
    Route::match(['get', 'post'], 'create', [StudentsController::class, 'create'])->name('hall_admin.student.create');
});
