<?php

use App\Http\Controllers\SuperAdmin\DashboardController;
use App\Http\Controllers\SuperAdmin\HallsController;
use App\Http\Controllers\SuperAdmin\UsersController;
use Illuminate\Support\Facades\Route;

Route::get('', [DashboardController::class, 'index'])->name('super_admin.dashboard');

Route::prefix('halls')->group(function () {
    Route::get('index', [HallsController::class, 'index'])->name('super_admin.hall.index');
    Route::post('add-hall', [HallsController::class, 'store'])->name('super_admin.hall.store');
});

Route::prefix('users')->group(function () {
    Route::get('', [UsersController::class, 'index'])->name('super_admin.user.index');
//    Route::post('add-user', [HallsController::class, 'store'])->name('super_admin.user.store');
});
