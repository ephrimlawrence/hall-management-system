<?php

use App\Http\Controllers\DeployController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/test', function () {
    return view('layouts.admin');
});

// Gitlab Webhook URL
Route::post('keeZmN1CB8s8YOvTBQ6wASa6b7F0xlLA-webhook', [DeployController::class, 'deploy']);


