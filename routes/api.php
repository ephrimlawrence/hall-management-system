<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ResourcesController;
use App\Http\Controllers\Api\StudentsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1'], function () {
    // Authentication
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::post('me', [AuthController::class, 'me']);
    });

    Route::group(['middleware' => ['auth:api']], function () {
        // Students
        Route::group(['prefix' => 'students'], function () {
            Route::get('', [StudentsController::class, 'all'])->name('api.student.get_all');
            Route::get('/{id}', [StudentsController::class, 'getById'])->name('api.student.get_by_id');
        });

        // Rooms
        Route::get('rooms', [ResourcesController::class, 'allRooms'])->name('api.room.all');
    });

});

