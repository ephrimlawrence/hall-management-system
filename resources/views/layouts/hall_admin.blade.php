<!doctype html>
<html class="no-js " lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ?? 'Hall Manager' }}</title>

    <link rel="icon" href="../admin/favicon.ico" type="image/x-icon"> <!-- Favicon-->

    <link rel="stylesheet" href="{{  asset('plugins/bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{  asset('plugins/jvectormap/jquery-jvectormap-2.0.3.min.css') }}"/>
    <link rel="stylesheet" href="{{  asset('plugins/charts-c3/plugin.css') }}"/>

    <link rel="stylesheet" href="{{  asset('plugins/morrisjs/morris.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/datatables.min.css') }}"/>

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
</head>

<body class="theme-blush">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="{{ asset('images/loader.svg') }}" width="48" height="48"
                                 alt="Aero">
        </div>
        <p>Please wait...</p>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="index.html"><img src="{{ asset('images/logo.svg') }}" width="25" alt="Aero"><span
                class="m-l-10">Aero</span></a>
    </div>

    {{-- Navigation   --}}
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <a class="image" href="../admin/profile.html"><img src="{{ asset('images/profile_av.jpg') }}"
                                                                       alt="User"></a>
                    <div class="detail">
                        <h4>Michael</h4>
                        <small>Hall Admin</small>
                    </div>
                </div>
            </li>

            <li class="active open"><a href="{{ route('hall_admin.dashboard') }}">
                    <i class="zmdi zmdi-home"></i><span>Dashboard</span></a>
            </li>

            <li>
                <a href="{{ route('hall_admin.room.index') }}"><i class="zmdi zmdi-apps"></i><span>Rooms</span></a>
            </li>
            <li>
                <a href="{{ route('hall_admin.floor.index') }}"><i class="zmdi zmdi-apps"></i><span>Floors</span></a>
            </li>

            <li>
                <a href="{{ route('hall_admin.staff.index') }}"><i class="zmdi zmdi-account"></i><span>Staffs</span></a>
            </li>

            <li>
                <a href="{{ route('hall_admin.student.index') }}"><i class="zmdi zmdi-account"></i><span>Students</span></a>
            </li>

        </ul>
    </div>
</aside>

<!-- Main Content -->

<section class="content">
    <div class="body_scroll">
        @include('layouts.flash_messages')

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @section('header')
            <div class="block-header">
                <div class="row">
                    <h2>{{ $header ?? 'Hall Administrator' }}</h2>
                </div>
            </div>
        @show

        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
</section>

@yield('modals')

<!-- Jquery Core Js -->
<script src="{{ asset('js/app.js') }}"></script>

<script src="{{ asset('js/bundles/libscripts.bundle.js') }}"></script>
<!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="{{ asset('js/bundles/vendorscripts.bundle.js') }}"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="{{ asset('js/bundles/jvectormap.bundle.js') }}"></script> <!-- JVectorMap Plugin Js -->
<script src="{{ asset('js/bundles/sparkline.bundle.js') }}"></script> <!-- Sparkline Plugin Js -->
<script src="{{ asset('js/bundles/c3.bundle.js') }}"></script>

<script src="{{ asset('js/bundles/mainscripts.bundle.js') }}"></script>

{{-- datatables --}}
<script type="text/javascript" src="{{ asset('vendor/datatables/datatables.min.js') }}"></script>
<script src="{{  asset('vendor/datatables/buttons.server-side.js') }}"></script>

<script src="{{ asset('js/admin.js') }}"></script>

@if (@$dataTable !== null)
    {!! $dataTable->scripts() !!}
@endif
</body>


</html>
