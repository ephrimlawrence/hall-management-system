<li>
    <div class="user-info">
        <a class="image" href="../admin/profile.html"><img src="assets/images/profile_av.jpg"
                                                           alt="User"></a>
        <div class="detail">
            <h4>Michael</h4>
            <small>Super Admin</small>
        </div>
    </div>
</li>

<li class="active open"><a href="{{ route('super_admin.dashboard') }}">
        <i class="zmdi zmdi-home"></i><span>Dashboard</span></a>
</li>

<li>
    <a href="{{ route('super_admin.hall.index') }}"><i class="zmdi zmdi-apps"></i><span>Halls</span></a>
</li>
<li>
    <a href="{{ route('super_admin.user.index') }}"><i class="zmdi zmdi-account"></i><span>Users</span></a>
</li>
