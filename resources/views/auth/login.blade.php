﻿<!doctype html>
<html class="no-js " lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>Hall Manager | Login</title>

    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Custom Css -->

    <link rel="stylesheet" href="{{  asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
</head>

<body class="theme-blush">

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <form class="card auth_form" method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="header">
                        <img class="logo" src="{{ asset('images/logo.svg') }}" alt="">
                        <h5>Log in</h5>
                    </div>
                    <div class="body">
                        <div class="input-group mb-3">
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                   placeholder="eg. mail@examp.com" value="{{ old('email') }}" required
                                   autocomplete="email" name="email" autofocus aria-describedby="Email">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="input-group mb-3">
                            <input type="password" class="form-control  @error('password') is-invalid @enderror"
                                   placeholder="Password" name="password" required autocomplete="current-password" aria-describedby="Password">

                            <div class="input-group-append">
                                <span class="input-group-text"><a href="forgot-password.html" class="forgot"
                                                                  title="Forgot Password"><i class="zmdi zmdi-lock"></i></a></span>
                            </div>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="checkbox">
                            <input id="remember_me" type="checkbox" type="checkbox"
                                   name="remember"{{ old('remember') ? 'checked' : '' }}>
                            <label for="remember_me">Remember Me</label>
                        </div>

                        <button type="submit" class="btn btn-primary btn-block waves-effect waves-light">SIGN
                            IN
                        </button>

                    </div>
                </form>
                <div class="copyright text-center">
                    &copy; {{ now()->year }}, Hall Manager. All Rights Reserved.
                </div>
            </div>

            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src="{{ asset('images/signin.svg') }}" alt="Sign In"/>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="{{ asset('js/app.js') }}"></script>
</body>


</html>
