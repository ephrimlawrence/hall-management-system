﻿@extends('layouts.hall_admin')

@section('content')
    <div class="">
        <div class="block-header">
            <div class="row justify-content-end">
                <button class="btn btn-success btn-icon float-right" type="button" data-toggle="modal"
                        data-target="#newRoomModal">
                    <i class="zmdi zmdi-plus"></i>
                </button>
            </div>
        </div>

        <div class="row clearfix row-cols-1 row-cols-md-2">
            <div class="card">
                <div class="body card-body">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>

    </div>
@endsection

@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="newRoomModal" tabindex="-1" aria-labelledby="newRoomModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('hall_admin.room.create') }}">
                    @csrf

                    <div class="modal-header">
                        <h5 class="modal-title" id="newRoomModalLabel">New Room</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="name" class="form-label">Room Name</label>
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="eg. E22" required value="{{ old('name') }}">
                        </div>

                        <div class="mb-3">
                            <label for="capacity" class="form-label">Capacity</label>
                            <input type="number" min="1" max="10" class="form-control" id="capacity"
                                   name="capacity"
                                   placeholder="eg. 2" required value="{{ old('capacity') }}">
                        </div>

                        <div class="mb-3">
                            <label for="block" class="form-label">Block</label>
                            <select id="block" name="block" class="form-control" required>
                                @foreach($blocks as $row)
                                    <option
                                        value="{{ $row->id }}" {{ $row->id == old('block') ? 'selected' : '' }}>{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="floor" class="form-label">Floor</label>
                            <select id="floor" name="floor" class="form-control" required>
                                @foreach($floors as $row)
                                    <option
                                        value="{{ $row->id }}" {{ $row->id == old('floor') ? 'selected' : '' }}>{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Room</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
