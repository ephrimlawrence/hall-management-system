﻿@extends('layouts.hall_admin')

@section('content')
    <div class="row clearfix">

        <div class="card">

            <div class="body card-body">

                <div class="row text-right justify-content-end mb-3">
                    <a href="{{ route('hall_admin.student.create') }}" class="btn btn-primary"><i
                            class="ti ti-plus"></i> Add Student
                    </a>
                </div>

                {!! $dataTable->table() !!}
            </div>

        </div>
    </div>
@endsection
