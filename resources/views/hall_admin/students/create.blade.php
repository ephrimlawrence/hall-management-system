﻿@extends('layouts.hall_admin')

@section('content')

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="card">

            <div class="body">
                <form id="form_validation" enctype="multipart/form-data" method="POST">
                    @csrf

                    <div class="form-row">
                        <div class="col form-group">
                            @php $val = !empty(old('first_name')) ? old('first_name') : $student->first_name @endphp

                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" placeholder="eg. John" name="first_name"
                                   value="{{ $val }}"
                                   id="first_name" required>
                        </div>
                        <div class="col form-group">
                            @php $val = !empty(old('other_names')) ? old('other_names') : $student->other_names @endphp

                            <label for="first_name">Other Name(s)</label>
                            <input type="text" class="form-control" placeholder="eg. Kwabena" name="other_names"
                                   value="{{ $val }}"
                                   id="other_names" required>
                        </div>
                        <div class="col form-group">
                            @php $val = !empty(old('last_name')) ? old('last_name') : $student->last_name @endphp

                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" placeholder="eg. Doe" name="last_name"
                                   value="{{ $val }}"
                                   id="last_name">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col form-group">
                            @php $val = !empty(old('programme')) ? old('programme') : $student->programme @endphp

                            <label for="programme">Programme</label>
                            <input type="text" class="form-control" placeholder="eg. B.Sc Social Science"
                                   name="programme"
                                   value="{{ $val }}"
                                   id="programme">
                        </div>
                        <div class="col form-group">
                            @php $val = !empty(old('department')) ? old('department') : $student->department @endphp

                            <label for="department">Department</label>
                            <input type="text" class="form-control" placeholder="eg. Department of Social Science"
                                   name="department"
                                   value="{{ $val }}"
                                   id="department">
                        </div>

                        <div class="col form-group">
                            @php $val = !empty(old('level')) ? old('level') : $student->level @endphp

                            <label for="level">Level</label>
                            <input type="number" min="100" max="900" step="100" class="form-control"
                                   placeholder="eg. 100"
                                   name="level"
                                   value="{{ $val }}"
                                   id="level" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col form-group">
                            @php $val = !empty(old('index_number')) ? old('index_number') : $student->index_number @endphp

                            <label for="index_number">Index Number</label>
                            <input type="text" class="form-control" placeholder="eg. PS/ITC/18/0025"
                                   name="index_number"
                                   value="{{ $val }}"
                                   id="index_number" required>
                            {{--                            TODO: add index number validation patterrn =>  pattern="\w\w/\w\w/\d\d/\d{1,6}" --}}
                        </div>

                        <div class="col form-group">
                            @php $val = !empty(old('room_id')) ? old('room_id') : $student->room_id @endphp

                            <label for="room_id">Room</label>
                            <select name="room_id" id="room_id" class="form-control" required>
                                @foreach($rooms as $room)
                                    <option
                                        value="{{ $room->id }}" {{ $room->id == $val ? 'selected' : '' }}>{{ $room->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col form-group">
                            @php $val = !empty(old('phone')) ? old('phone') : $student->phone @endphp

                            <label for="phone">Phone Number</label>
                            <input type="tel" name="phone" class="form-control"
                                   value="{{ $val }}" id="phone" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col form-group">
                            @php $val = !empty(old('move_in_date')) ? old('move_in_date') : $student->move_in_date @endphp

                            <label for="move_in_date">Move In Date</label>
                            <input type="date" name="move_in_date" class="form-control"
                                   value="{{ $val }}" id="move_in_date" required>
                        </div>

                        <div class="col form-group">
                            @php $val = !empty(old('move_out_date')) ? old('move_out_date') : $student->move_out_date @endphp

                            <label for="move_out_date">Move Out Date</label>
                            <input type="date" name="move_out_date" class="form-control"
                                   value="{{ $val }}" id="move_out_date" required>
                        </div>

                        <div class="col-md-4 form-group  mt-4">
                            <div class="custom-file">
                                <input type="file" value="{{ old('image') }}" class="custom-file-input"
                                       accept="image/x-png,image/jpeg" name="image"
                                       id="customFile">
                                <label class="custom-file-label" for="customFile">Choose Image</label>
                            </div>

                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <button class="btn btn-raised btn-primary waves-effect " type="submit">SAVE STUDENT
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
