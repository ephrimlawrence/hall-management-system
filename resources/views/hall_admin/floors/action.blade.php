<div class="btn-group">
    <a href="#" class="btn btn-primary btn-sm me-2 mr-2"><i
            class="ti-pencil"></i></a>
    <a href="{{ route('hall_admin.floor.delete', ['id' => $row->id]) }}"
       onclick="return confirm('Do you want to delete this room?')" class="btn btn-danger btn-sm"><i
            class="ti-trash"></i></a>
</div>
