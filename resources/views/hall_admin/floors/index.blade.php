﻿@extends('layouts.hall_admin')

@section('content')
    <div class="">
        <div class="block-header">
            <div class="row justify-content-end">
                <button class="btn btn-success btn-icon float-right" type="button" data-toggle="modal"
                        data-target="#newFloorModal">
                    <i class="zmdi zmdi-plus"></i>
                </button>
            </div>
        </div>

        <div class="row clearfix row-cols-1 row-cols-md-2">
            <div class="card">
                <div class="body card-body">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>

    </div>
@endsection

@section('modals')
    <!-- Modal -->
    <div class="modal fade" id="newFloorModal" tabindex="-1" aria-labelledby="newFloorModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('hall_admin.floor.create') }}">
                    @csrf

                    <div class="modal-header">
                        <h5 class="modal-title" id="newFloorModalLabel">New Floor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="eg. E22" required value="{{ old('name') }}">
                        </div>

                        <div class="mb-3">
                            <label for="block" class="form-label">Block</label>
                            <select id="block" name="block" class="form-control" required>
                                @foreach($blocks as $row)
                                    <option
                                        value="{{ $row->id }}" {{ $row->id == old('block') ? 'selected' : '' }}>{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save Floor</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
