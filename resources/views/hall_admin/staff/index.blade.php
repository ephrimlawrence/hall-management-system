﻿@extends('layouts.hall_admin')

@section('content')
    <div class="row clearfix">

        <div class="card">

            <div class="body card-body">

                <div class="row text-right justify-content-end mb-3 me-2">
                    <a href="{{ route('hall_admin.staff.create') }}" class="btn btn-primary"><i
                            class="ti ti-plus"></i> Add Staff
                    </a>
                </div>

                {!! $dataTable->table() !!}
            </div>

        </div>
    </div>
@endsection
