﻿@extends('layouts.hall_admin')

@section('content')
    <div class="row clearfix">
        <div class="card">

            <div class="body">
                <form id="form_validation" method="POST">
                    @csrf

                    <div class="form-group form-float">
                        @php $val = !empty(old('name')) ? old('name') : $user->name @endphp

                        <label for="name">Staff Name</label>
                        <input type="text" class="form-control" placeholder="Name" name="name" value="{{ $val }}"
                               id="name" required>
                    </div>

                    <div class="form-group form-float">
                        @php $val = !empty(old('email')) ? old('email') : $user->email @endphp

                        <label for="email">Staff Email</label>
                        <input type="email" class="form-control" placeholder="Email" id="email" value="{{ $val }}"
                               name="email"
                               required>
                    </div>

                    <div class="form-group form-float">
                        @php $val = !empty(old('role')) ? old('role') : $user->role_id @endphp

                        <label for="role">Staff Role</label>
                        <select name="role" id="role" class="form-control" required>
                            @foreach($roles as $role)
                                <option
                                    value="{{ $role->id }}" {{ $role->id == $val ? 'selected' : '' }}>{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="row justify-content-center">
                        <button class="btn btn-raised btn-primary waves-effect" type="submit">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
