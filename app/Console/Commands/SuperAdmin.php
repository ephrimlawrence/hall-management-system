<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'super-admin:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new Super Admin account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() : int
    {
        $password = Str::random(15);
        $role = Role::query()->where(DB::raw("LOWER(name)"), 'like', '%super admin%')
                    ->first();
        $name = $this->ask('Full Name');
        $email = $this->ask('Email ');

        $rules = Validator::make(['name' => $name, 'email' => $email], [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|min:5'
        ]);

        if ($rules->fails()) {
            $errors = $rules->errors();
            foreach ($errors->getMessages() as $key => $message) {
                $this->error($message[0]);
            }
            return 1;
        }

        if ($this->confirm('Do you want to create account?')) {
            User::query()->create([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
                'role_id' => $role->id
            ]);

            $this->info('Super admin created successfully!');
            $this->line("Email:     {$email}");
            $this->line("Password:  {$password}");
        }
        return 0;
    }
}
