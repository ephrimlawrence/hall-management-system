<?php

namespace App\Transformers;

use App\Models\Floor;
use Flugg\Responder\Transformers\Transformer;

class FloorTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = ['block'];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param \App\Models\Floor $floor
     * @return array
     */
    public function transform(Floor $floor)
    {
        return [
            'id' => (int) $floor->id,
            'name' => $floor->name
        ];
    }
}
