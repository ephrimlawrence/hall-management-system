<?php

namespace App\Transformers;

use App\Models\Block;
use Flugg\Responder\Transformers\Transformer;

class BlockTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = ["rooms", "floors"];

    /**
     * List of auto loaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param Block $block
     * @return array
     */
    public function transform(Block $block)
    {
        return [
            'id' => (int) $block->id,
            'name' => $block->name
        ];
    }
}
