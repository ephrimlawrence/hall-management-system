<?php

namespace App\Transformers;

use App\Models\Room;
use Flugg\Responder\Transformers\Transformer;

class RoomTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [
        "floor" => FloorTransformer::class,
        "block" => BlockTransformer::class,
    ];

    /**
     * Transform the model.
     *
     * @param \App\Models\Room $room
     * @return array
     */
    public function transform(Room $room)
    {
        return [
            'id' => (int) $room->id,
            'capacity' => (int) $room->capacity,
            'name' => $room->name
        ];
    }
}
