<?php

namespace App\Transformers;

use App\Models\Student;
use Flugg\Responder\Transformers\Transformer;

class StudentTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of auto loaded default relations.
     *
     * @var array
     */
    protected $load = [
        'room' => RoomTransformer::class,
    ];

    /**
     * Transform the model.
     *
     * @param Student $student
     * @return array
     */
    public function transform(Student $student)
    {
        return [
            'id' => (int) $student->id,
            'image' => $student->image_url,
            'firstName' => $student->first_name,
            'otherNames' => $student->other_names,
            'lastName' => $student->last_name,
            'fullName' => $student->full_name,
            'level' => $student->level,
            'programme' => $student->programme,
            'phone' => $student->phone,
            'floor' => @$student->room->floor->name,
            'department' => $student->department,
            'indexNumber' => $student->index_number,
            'moveInDate' => $student->move_out_date,
            'moveOutDate' => $student->move_in_date,
            'createdAt' => $student->created_at,
        ];
    }
}
