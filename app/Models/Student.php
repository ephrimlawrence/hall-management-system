<?php

namespace App\Models;

use App\Helpers\StorageHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "students";

    protected $fillable = [
        'first_name', 'other_names', 'last_name', 'level', 'staff_id',
        'programme', 'department', 'image', 'index_number', 'room_id',
        'hall_id', 'phone', 'move_out_date', 'move_in_date'
    ];

    protected $casts = [
        'move_out_date' => 'date',
        'move_in_date' => 'date',
    ];

    public function room() : BelongsTo
    {
        return $this->belongsTo(Room::class);
    }

    public function hall() : BelongsTo
    {
        return $this->belongsTo(Hall::class);
    }

    public function staff() : BelongsTo
    {
        return $this->belongsTo(Staff::class);
    }

    public function getImageUrlAttribute() : string
    {
        return StorageHelper::b2url($this->image);
    }

    public function getFullNameAttribute() : string
    {
        return "{$this->first_name} {$this->other_names} {$this->last_name}";
    }
}
