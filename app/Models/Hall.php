<?php

namespace App\Models;

use App\Helpers\StorageHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hall extends Model
{
    use SoftDeletes;

    protected $table = 'halls';

    protected $fillable = ['logo', 'name', 'description', 'created_by'];

    public function created_by() : BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function admin() : BelongsTo
    {
        return $this->belongsTo(User::class, 'admin_id');
    }

    public function blocks() : HasMany
    {
        return $this->hasMany(Block::class);
    }

    public function rooms() : HasManyThrough
    {
        return $this->hasManyThrough(Room::class, Block::class);
    }

    public function floors() : HasManyThrough
    {
        return $this->hasManyThrough(Floor::class, Block::class);
    }

    public function students() : HasMany
    {
        return $this->hasMany(Student::class);
    }

    public function staffs() : HasMany
    {
        return $this->hasMany(Staff::class);
    }

    public function getLogoUrlAttribute() : string
    {
        return StorageHelper::b2url($this->logo);
    }
}
