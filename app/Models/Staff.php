<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use HasFactory, SoftDeletes;


    protected $table = "staffs";

    protected $fillable = ['account_id', 'hall_id'];

    public function account() : BelongsTo
    {
        return $this->belongsTo(User::class, 'account_id');
    }

    public function hall() : BelongsTo
    {
        return $this->belongsTo(Hall::class);
    }
}
