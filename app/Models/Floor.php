<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Floor extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "floors";

    protected $fillable = ['name', 'block_id'];

    public function block() : BelongsTo
    {
        return $this->belongsTo(Block::class);
    }

    public function rooms() : HasMany
    {
        return $this->hasMany(Room::class);
    }
}
