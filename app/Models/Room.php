<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "rooms";

    protected $fillable = ['name', 'capacity', 'block_id', 'floor_id'];

    public function block() : BelongsTo
    {
        return $this->belongsTo(Block::class);
    }

    public function floor() : BelongsTo
    {
        return $this->belongsTo(Floor::class);
    }

    /**
     * Returns the total space left for additional students
     *
     * @return int
     */
    public function getSpaceLeftAttribute() : int
    {
        //TODO: fetch for only students occupied in the room for the current academic year
        return $this->capacity - $this->space_occupied;
    }

    /**
     * Returns the total space occupied by students
     *
     * @return int
     */
    public function getSpaceOccupiedAttribute() : int
    {
        //TODO: fetch for only students occupied in the room for the current academic year
        return $this->students()->count();
    }

    public function students() : HasMany
    {
        return $this->hasMany(Student::class);
    }
}
