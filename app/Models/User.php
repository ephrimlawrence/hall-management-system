<?php

namespace App\Models;

use App\Helpers\StorageHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use JetBrains\PhpStorm\Pure;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'role_id', 'image'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['email_verified_at' => 'datetime'];

    public function role() : BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public function getHallAttribute() : Hall|null
    {
        return $this->staff->hall;
    }

    public function staff() : HasOne
    {
        return $this->hasOne(Staff::class, 'account_id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() : array
    {
        return [];
    }

    #[Pure] public function getIsSuperAdminAttribute() : bool
    {
        return false !== stripos($this->role->name, "super admin");
    }

    #[Pure] public function getIsHallAdminAttribute() : bool
    {
        return false !== stripos($this->role->name, "hall admin");
    }

    #[Pure] public function getIsStaffAttribute() : bool
    {
        return false !== stripos($this->role->name, "staff");
    }

    public function getImageUrlAttribute() : string
    {
        return StorageHelper::b2url($this->image);
    }
}
