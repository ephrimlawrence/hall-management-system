<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Block extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "blocks";

    protected $fillable = ['name', 'hall_id'];

    public function hall() : BelongsTo
    {
        return $this->belongsTo(Hall::class);
    }

    public function rooms() : HasMany
    {
        return $this->hasMany(Room::class);
    }

    public function floors() : HasMany
    {
        return $this->hasMany(Floor::class);
    }
}
