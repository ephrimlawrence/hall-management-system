<?php

namespace App\Helpers;

class StorageHelper
{

    /*
     * Generate backblaze url for the given path
     */
    public static function b2url(string $path) : string
    {
        if (str_contains($path, 'via.placeholder.com')) {
            return $path;
        }
        if (preg_match('/http|https/i', $path)) {
            return $path;
        }

        if (config('filesystems.default') === 'b2') {
            $bucket = config('filesystems.b2bucketName');
            $url = config('filesystems.b2url');
            $path = "{$url}/{$bucket}/{$path}";
        }
        return $path;
    }
}
