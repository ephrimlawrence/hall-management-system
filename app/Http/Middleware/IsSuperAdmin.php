<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) : mixed
    {
        $user = auth()->user();
        if ($user !== null && $user->is_super_admin) {
            return $next($request);
        }
        abort(404);
    }
}
