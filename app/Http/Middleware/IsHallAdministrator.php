<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsHallAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();
        if ($user !== null && $user->is_hall_admin) {
            return $next($request);
        }
        abort(404);
    }
}
