<?php

namespace App\Http\Controllers;

use App\Models\Hall;
use App\Models\Staff;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function isPostRequest() : bool
    {
        return request()->method() === 'POST';
    }

    /**
     * Returns all the rooms in the hall
     *
     * @return HasManyThrough
     */
    public function rooms() : HasManyThrough
    {
        return $this->hall()->rooms()->orderBy('name');
    }

    /**
     * Return the hall that the current authenticated user belongs to
     * @return Hall
     */
    public function hall() : Hall
    {
        return @$this->staff()->hall;
    }

    /**
     * Returns the staff object of the current authenticated user
     *
     * @return Staff
     */
    public function staff() : Staff
    {
        $guard = request()->is('api/*') ? 'api' : 'web';
        return auth($guard)->user()->staff;
    }

    public function currentStudents() : HasMany
    {
        return $this->hall()->students()->whereDate('move_in_date', '<=', now())
                    ->whereDate('move_out_date', '>=', now());
    }
}
