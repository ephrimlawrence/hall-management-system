<?php

namespace App\Http\Controllers\HallAdmin;

use App\DataTables\HallAdmin\RoomsDataTable;
use App\Models\Room;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoomsController extends Controller
{
    public function index(RoomsDataTable $table)
    {
        return $table->render('hall_admin.rooms.index', [
            'blocks' => $this->hall()->blocks()->get(),
            'floors' => $this->hall()->floors()->get(),
            'header' => 'Rooms'
        ]);
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => [
                'required', 'string',
                Rule::unique('rooms')->where(function ($query) use ($request) {
                    return $query->where('name', $request->name);
                }),
            ],
            'capacity' => 'required|numeric|min:0|max:10',
            'block' => 'required|exists:blocks,id',
            'floor' => 'required|exists:floors,id'
        ]);

        $this->hall()->rooms()->create([
            'name' => $request->name,
            'capacity' => $request->capacity,
            'block_id' => $request->block,
            'floor_id' => $request->floor
        ]);

        return back()->with('success', 'Room created successfully');
    }

    public function delete(int $id) : RedirectResponse
    {
        $room = Room::query()->findOrFail($id);
        $room->delete();

        return back()->with('success', 'Room delete successfully');
    }
}
