<?php

namespace App\Http\Controllers\HallAdmin;

use App\DataTables\HallAdmin\StudentsDataTable;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function index(StudentsDataTable $table)
    {
        return $table->render('hall_admin.students.index', [
            'header' => 'Students'
        ]);
    }

    public function create(Request $request)
    {
        if ($this->isPostRequest()) {
            $request->validate([
                'room_id' => 'nullable|exists:rooms,id',
                'first_name' => 'required|string|min:2',
                'other_names' => 'required|string|min:2',
                'last_name' => 'required|string|min:2',
                'index_number' => 'required', //TODO: add pattern
                'image' => 'nullable|image|max:5000',
                'programme' => 'nullable|string|min:10',
                'department' => 'nullable|string|min:10',
                'level' => 'required|numeric|min:100,max:900',
                'move_in_date' => 'required|date|before:move_out_date',
                'move_out_date' => 'required|date|after:move_in_date',
            ]);

            $path = '';
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $ext = $request->image->extension();
                    $path = $request->image->storeAs('students', "{$request->index_number}.{$ext}");
                }
            }
            $inputs = $request->all();
            $inputs['image'] = $path;
            $inputs['hall_id'] = $this->hall()->id;
            $inputs['staff_id'] = auth()->user()->id;

            Student::query()->create($inputs);
            return back()->with('success', 'Student created successfully');
        }

        return view('hall_admin.students.create', [
            'header' => 'New Student',
            'user' => new User(),
            'student' => new Student(),
            'rooms' => $this->rooms()->get()
        ]);
    }
}
