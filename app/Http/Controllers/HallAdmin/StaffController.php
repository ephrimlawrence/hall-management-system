<?php

namespace App\Http\Controllers\HallAdmin;

use App\DataTables\HallAdmin\StaffsDataTable;
use App\Models\Role;
use App\Models\Staff;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class StaffController extends Controller
{
    public function index(StaffsDataTable $table)
    {
        return $table->render('hall_admin.staff.index');
    }

    public function create(Request $request)
    {
        $roles = Role::where('name', '!=', 'Super Admin')->get();
        if ($this->isPostRequest()) {
            $request->validate([
                'name' => 'required|min:5',
                'role' => 'required|exists:roles,id',
                'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->where(function ($query) use ($request) {
                        return $query->where('email', $request->email);
                    }),
                ]
            ]);

//            TODO: automatically set hall id based on logged in user
//            TODO: automatically send password as email to registered user
//            TODO: automatically alert super admin that staff has been created

            $password = Str::random(10);
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($password)
            ]);
            Staff::create([
                'hall_id' => $this->hall()->id,
                'account_id' => $user->id
            ]);
            session()->flash('success', 'Staff created successfully. An email with credentials has also been sent to the staff');
        }

        return view('hall_admin.staff.create', [
            'roles' => $roles,
            'user' => new User(),
            'header' => 'New Staff'
        ]);
    }

}
