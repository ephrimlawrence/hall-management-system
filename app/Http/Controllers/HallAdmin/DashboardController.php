<?php

namespace App\Http\Controllers\HallAdmin;


class DashboardController extends Controller
{
    public function index()
    {
        return view('hall_admin.dashboard');
    }
}
