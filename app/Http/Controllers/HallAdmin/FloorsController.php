<?php

namespace App\Http\Controllers\HallAdmin;

use App\DataTables\HallAdmin\FloorsDataTable;
use App\Models\Floor;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class FloorsController extends Controller
{
    public function index(FloorsDataTable $table)
    {
        return $table->render('hall_admin.floors.index', [
            'blocks' => $this->hall()->blocks()->get(),
            'header' => 'Floors'
        ]);
    }

    public function create(Request $request) : RedirectResponse
    {
        $request->validate([
            'name' => 'required|string',
            'block' => 'required|exists:blocks,id',
        ]);

        Floor::query()->create([
            'name' => $request->name,
            'block_id' => $request->block,
        ]);

        return back()->with('success', 'Floor created successfully');
    }

    public function delete(int $id) : RedirectResponse
    {
        $item = Floor::query()->findOrFail($id);
        $item->delete();

        return back()->with('success', 'Floor delete successfully');
    }
}
