<?php

namespace App\Http\Controllers\SuperAdmin;

use App\DataTables\SuperAdmin\HallsDataTable;
use App\Models\Hall;
use App\Models\User;
use Illuminate\Http\Request;

class HallsController extends Controller
{
    public function index(HallsDataTable $table)
    {
        return $table->render('super_admin.halls.index', [
            'users' => User::all() //TODO: get only users with role hall-admin
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'administrator' => 'required|exists:users,id',
            'logo' => 'nullable|image',
            'description' => 'nullable|string',
        ]);

        $path = '';
        if ($request->file('logo')->isValid()) {
            $ext = $request->logo->extension();
            $path = $request->logo->storeAs('logos', "{$request->name}.{$ext}");
        }

        $hall = Hall::create([
            'name' => $request->name,
            'logo' => $path,
            'admin_id' => $request->administrator,
            'description' => $request->description
        ]);

        return back()->with('success', 'Hall created successfully');
    }
}
