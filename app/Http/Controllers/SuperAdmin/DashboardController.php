<?php

namespace App\Http\Controllers\SuperAdmin;

class DashboardController extends Controller
{
    public function index()
    {
        return view('super_admin.dashboard');
    }
}
