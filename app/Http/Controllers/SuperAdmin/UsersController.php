<?php

namespace App\Http\Controllers\SuperAdmin;

use App\DataTables\SuperAdmin\HallsDataTable;
use App\DataTables\SuperAdmin\UsersDataTable;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(UsersDataTable $table)
    {
        return $table->render('super_admin.users.index', [
//            'users' => User::all()
        ]);
    }
}
