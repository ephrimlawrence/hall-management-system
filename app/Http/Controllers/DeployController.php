<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

class DeployController extends Controller
{
    public function deploy(Request $request) : JsonResponse
    {
        logger('Gitlab Webhook received');

        $mode = $request->query('mode', 'dev');
        $gitlabToken = $request->header('X-Gitlab-Token');

        $hashKey = Str::random(100);
        $gitlabHash = hash_hmac('sha256', $gitlabToken, $hashKey, false);
        $localHash = hash_hmac('sha256', config('app.deploy_secret'), $hashKey, false);
        $args = $mode === 'production' ? 'production' : 'dev';

        if (hash_equals($gitlabHash, $localHash)) {
            Process::fromShellCommandline("/usr/bin/bash deploy.sh $args >> storage/logs/deploy.log 2>&1", base_path(), null, null, null,)->start();

            logger('Gitlab Webhook processing completed');
            //TODO: implement sending of email alert
        }


        return response()->json(['status' => 'success']);
    }
}
