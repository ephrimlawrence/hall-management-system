<?php

namespace App\Http\Controllers\Api;

use App\Transformers\StudentTransformer;
use Flugg\Responder\Responder;
use Illuminate\Http\JsonResponse;

class StudentsController extends Controller
{
    public function all(Responder $responder) : JsonResponse
    {
        return $responder->success($this->currentStudents(), StudentTransformer::class)->respond();
    }

    public function getById(Responder $responder, int $id) : JsonResponse
    {
        $student = $this->currentStudents()->where('id', $id);
        return $responder->success($student, StudentTransformer::class)->respond();
    }
}
