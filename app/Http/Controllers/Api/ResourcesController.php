<?php

namespace App\Http\Controllers\Api;

use App\Transformers\RoomTransformer;
use Flugg\Responder\Responder;
use Illuminate\Http\JsonResponse;

class ResourcesController extends Controller
{
    public function allRooms(Responder $responder) : JsonResponse
    {
        return $responder->success($this->rooms(), RoomTransformer::class)->respond();
    }
}
