<?php

namespace App\DataTables\HallAdmin;

use App\Models\Student;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class StudentsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('image', function ($row) {
                return "<img src='{$row->image_url}' class='img rounded-circle' alt='image' width='100px'>";
            })
            ->addColumn('name', function ($row) {
                return $row->full_name;
            })
            ->addColumn('room', function ($row) {
                return @$row->room->name ?? 'Unassigned';
            })
            ->addColumn('floor', function ($row) {
                return @$row->room->floor->name ?? 'N/A';
            })
            ->addColumn('created_at', function ($row) {
                return $row->created_at->format('D, M, Y');
            })
            ->addColumn('moved_in_date', function ($row) {
                return $row->move_in_date === null ? 'N/A' : $row->move_in_date->format('D, M, Y');
            })
            ->addColumn('moved_out_date', function ($row) {
                return $row->move_out_date === null ? 'N/A' : $row->move_out_date->format('D, M, Y');
            })
            ->addColumn('updated_at', function ($row) {
                return $row->created_at->format('D, M, Y');
            })
            ->addColumn('action', function ($row) {
                return view('hall_admin.staff.action', ['row' => $row]);
            })
            ->rawColumns(['action', 'image']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Student $model
     * @return Builder
     */
    public function query(Student $model) : Builder
    {
        return $model->newQuery()
                     ->where('hall_id', auth()->user()->hall->id)->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() : \Yajra\DataTables\Html\Builder
    {
        return $this->builder()
                    ->setTableId('table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() : array
    {
        return [
            'image' => ['width' => 100, 'title' => ''],
            'name' => ['width' => 300],
            'index_number' => ['width' => 300],
            'room' => ['width' => 300],
            'floor' => ['width' => 300],
            'phone' => ['width' => 300],
            'moved_in_date' => ['width' => 300],
            'moved_out_date' => ['width' => 300],
            'created_at' => ['width' => 150, 'title' => 'Date Created'],
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() : string
    {
        return 'students' . date('YmdHis');
    }
}
