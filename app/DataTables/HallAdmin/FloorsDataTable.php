<?php

namespace App\DataTables\HallAdmin;

use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class FloorsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('name', function ($row) {
                return $row->name;
            })
            ->addColumn('block', function ($row) {
                return $row->block->name;
            })
            ->addColumn('rooms', function ($row) {
                return $row->rooms()->count();
            })
            ->addColumn('created_at', function ($row) {
                return $row->created_at->format('D, M, Y');
            })
            ->addColumn('action', function ($row) {
                return view('hall_admin.floors.action', ['row' => $row]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @return Builder
     */
    public function query() : mixed
    {
        return auth()->user()->hall->floors();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() : \Yajra\DataTables\Html\Builder
    {
        return $this->builder()
                    ->setTableId('table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() : array
    {
        return [
            'name' => ['width' => 300, 'searchable' => true],
            'block' => ['width' => 300],
            'rooms' => ['width' => 300, 'title' => 'Total Room'],
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() : string
    {
        return 'floors_' . date('YmdHis');
    }
}
