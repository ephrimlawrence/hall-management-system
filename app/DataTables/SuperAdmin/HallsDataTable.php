<?php

namespace App\DataTables\SuperAdmin;

use App\Models\Hall;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class HallsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('logo', function ($row) {
                return "<img src='{$row->logo_url}' class='rounded' alt='{$row->name} logo'>";
            })
            ->addColumn('created_at', function ($row) {
                return $row->created_at->format('D, M, Y');
            })
            ->addColumn('updated_at', function ($row) {
                return $row->created_at->format('D, M, Y');
            })
            ->addColumn('action', function ($row) {
                return view('super_admin.halls.action', ['row' => $row]);
            })
            ->rawColumns(['action', 'logo']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Hall $model
     * @return Builder
     */
    public function query(Hall $model) : Builder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() : \Yajra\DataTables\Html\Builder
    {
        return $this->builder()
                    ->setTableId('halls-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() : array
    {
        return [
            Column::computed('logo')
                  ->width(80)
                  ->addClass('text-center'),
            'name' => ['width' => 300],
            'created_at' => ['width' => 150],
            'updated_at' => ['width' => 150],
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() : string
    {
        return 'halls' . date('YmdHis');
    }
}
