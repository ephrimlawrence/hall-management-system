<?php

namespace App\DataTables\SuperAdmin;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('role', function ($row) {
                return @$row->role->name ?? 'N/A';
            })
            ->addColumn('hall', function ($row) {
                return @$row->hall->name ?? 'N/A';
            })
            ->addColumn('created_at', function ($row) {
                return $row->created_at->format('D, M, Y');
            })
            ->addColumn('updated_at', function ($row) {
                return $row->created_at->format('D, M, Y');
            })
            ->addColumn('action', function ($row) {
                return view('super_admin.users.action', ['row' => $row]);
            })
            ->rawColumns(['action', 'logo']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param User $model
     * @return Builder
     */
    public function query(User $model) : Builder
    {
//        TODO: query for only users of the current hall
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() : \Yajra\DataTables\Html\Builder
    {
        return $this->builder()
                    ->setTableId('table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() : array
    {
        return [
            'name' => ['width' => 300],
            'email' => ['width' => 300],
            'hall' => ['width' => 300],
            'role' => ['width' => 300],
            'created_at' => ['width' => 150],
            'updated_at' => ['width' => 150],
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() : string
    {
        return 'users_' . date('YmdHis');
    }
}
